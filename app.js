const express = require('express');
const app = express();

const productsRoutes = require('./api/routes/products');
const orderRoutes = require('./api/routes/orders');

app.use('/products', productsRoutes);
app.use('/orders', orderRoutes);
app.use('/', (req, res, next) => {
  res.status(200).json({
    message: 'Hello, welcome to the Api!'
  });
});

app.use((req, res, next) => {
  const error = new Error('not found');
  error.status(404);
  next(error);
});

app.use((error, req, res, next) => {
  
});

module.exports = app;
